#!/bin/bash

curl -s -L -o /var/www/theme.zip \
        https://github.com/omeka-s-themes/$1/releases/download/v$2/$1-v$2.zip \
    && mkdir /var/www/theme \
    && unzip -q /var/www/theme.zip -d /var/www/theme \
    && rm /var/www/theme.zip \
    && mv /var/www/theme/* /var/www/html/themes \
    && rmdir /var/www/theme
