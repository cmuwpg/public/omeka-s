#!/bin/bash

echo Installing $1

curl -s -L -o /var/www/module.zip "$1" \
    && mkdir /var/www/module \
    && unzip -q /var/www/module.zip -d /var/www/module \
    && rm /var/www/module.zip \
    && mv /var/www/module/* /var/www/html/modules \
    && rmdir /var/www/module
