#!/bin/bash

if [ -v OMEKA_S_DB_USER -a -v OMEKA_S_DB_PASSWORD -a -v OMEKA_S_DB_NAME -a -v OMEKA_S_DB_HOST ]; then
  cat << EOS > /var/www/html/config/database.ini
user     = "$OMEKA_S_DB_USER"
password = "$OMEKA_S_DB_PASSWORD"
dbname   = "$OMEKA_S_DB_NAME"
host     = "$OMEKA_S_DB_HOST"
EOS
fi

chown www-data:www-data /var/www/html/files
chmod 0770 /var/www/html/files

exec "$@"
