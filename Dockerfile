FROM php:7.4-apache

MAINTAINER Ryan Rempel <rgrempel@cmu.ca>

ENV OMEKA_VERSION=2.1.2 \
    THEME_CENTERROW=1.5.0 \
    THEME_COZY=1.4.0 \
    THEME_PAPERS=1.1.0 \
    THEME_THEDAILY=1.5.0
    
RUN a2enmod rewrite

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
	curl libzip-dev unzip libfreetype6-dev libjpeg62-turbo-dev \
	libmcrypt-dev libpng-dev libjpeg-dev libmemcached-dev \
	zlib1g-dev imagemagick libmagickwand-dev \
        catdoc docx2txt lynx odt2txt poppler-utils

# Install the PHP extensions we need
RUN docker-php-ext-install -j$(nproc) pdo_mysql mysqli gd zip exif

RUN curl -s -L -o /var/www/omeka-s.zip \
       https://github.com/omeka/omeka-s/releases/download/v${OMEKA_VERSION}/omeka-s-${OMEKA_VERSION}.zip \
    && unzip -q /var/www/omeka-s.zip -d /var/www \
    && rm /var/www/omeka-s.zip \
    && rm -rf /var/www/html \
    && mv /var/www/omeka-s /var/www/html

COPY install-theme.sh /install-theme.sh

RUN /install-theme.sh centerrow $THEME_CENTERROW 
RUN /install-theme.sh cozy $THEME_COZY 
RUN /install-theme.sh papers $THEME_PAPERS 
RUN /install-theme.sh thedaily $THEME_THEDAILY 

COPY install-module.sh /install-module.sh
COPY module-list /module-list 

RUN cat /module-list | xargs -n1 /install-module.sh

RUN cp /var/www/html/config/local.config.php /var/www/html/config/local.config.php.dist

COPY imagemagick-policy.xml /etc/ImageMagick/policy.xml
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
